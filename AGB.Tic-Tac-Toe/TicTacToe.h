#pragma once
#include <iostream>

class TicTacToe
{
private:
	/*char m_board
	{

	}
	int m_numTurns
	{

	}
	char m_playerTurn
	{

	}
	char m_winner
	{

	}*/


public:
	char num[10] = { ' ','1','2', '3', '4', '5', '6', '7', '8', '9' };

	int count = 1;
	char player1 = '1(X)';
	char player2 = '2(O)';
	char piece;
	int winner = 1;

	void DisplayBoard()//works
	{
		std::cout << "Player 1 (X), Player 2 (O)" << "\n";
		std::cout << " " << num[1] << " | " << num[2] << " | " << num[3] << " \n";
		std::cout << "___|___|___\n";
		std::cout << " " << num[4] << " | " << num[5] << " | " << num[6] << " \n";
		std::cout << "___|___|___\n";
		std::cout << " " << num[7] << " | " << num[8] << " | " << num[9] << " \n";
		std::cout << "   |   |   \n";
	}

	bool IsOver()
	{
		if (num[1] == 'X' && num[2] == 'X' && num[3] == 'X' or
			num[4] == 'X' && num[5] == 'X' && num[6] == 'X' or
			num[7] == 'X' && num[8] == 'X' && num[9] == 'X' or
			num[1] == 'X' && num[4] == 'X' && num[7] == 'X' or
			num[2] == 'X' && num[5] == 'X' && num[8] == 'X' or
			num[3] == 'X' && num[6] == 'X' && num[9] == 'X' or
			num[1] == 'X' && num[5] == 'X' && num[9] == 'X' or
			num[3] == 'X' && num[6] == 'X' && num[7] == 'X')
		{
			count = 3;
			return true;
		}
		else if (num[1] == 'O' && num[2] == 'O' && num[3] == 'O' or
			num[4] == 'O' && num[5] == 'O' && num[6] == 'O' or
			num[7] == 'O' && num[8] == 'O' && num[9] == 'O' or
			num[1] == 'O' && num[4] == 'O' && num[7] == 'O' or
			num[2] == 'O' && num[5] == 'O' && num[8] == 'O' or
			num[3] == 'O' && num[6] == 'O' && num[9] == 'O' or
			num[1] == 'O' && num[5] == 'O' && num[9] == 'O' or
			num[3] == 'O' && num[6] == 'O' && num[7] == 'O')
		{
			count = 2;
			return true;
		}
		else if (num[1] != '1' && num[2] != '2' && num[3] != '3' &&
			num[4] != '4' && num[5] != '5' && num[6] != '6' &&
			num[7] != '7' && num[8] != '8' && num[9] != '9')
		{
			return true;
		}
		else
		{
			return false;
		}
		return 0;
	}

	char GetPlayerTurn()//works
	{
		if (count % 2 == 0)
		{
			count++;
			piece = 'O';
			return '2';
		}
		else if (count % 2 != 0)
		{
			count++;
			piece = 'X';
			return '1';
		}
	}

	bool IsValidMove(int number)
	{
		/*if (number = num[number])
		{
			++count;
			return true;
		}
		else
		{
			return false;
		}*/
		if (number == 1 && num[1] == '1')

			num[1] = piece;
		else if (number == 2 && num[2] == '2')

			num[2] = piece;
		else if (number == 3 && num[3] == '3')

			num[3] = piece;
		else if (number == 4 && num[4] == '4')

			num[4] = piece;
		else if (number == 5 && num[5] == '5')

			num[5] = piece;
		else if (number == 6 && num[6] == '6')

			num[6] = piece;
		else if (number == 7 && num[7] == '7')

			num[7] = piece;
		else if (number == 8 && num[8] == '8')

			num[8] = piece;
		else if (number == 9 && num[9] == '9')

			num[9] = piece;
		else
		{
			count--;
			return 0;
		}
	}

	void Move(int number)
	{
		if (count % 2 == 0)
		{
			num[number] = 'O';
		}
		else if (count % 2 != 0)
		{
			num[number] = 'X';
		}
	}

	void DisplayResult()
	{
		if (count == 2)
		{
			std::cout << "The Winner is Player 2(O): ";
		}
		else if (count == 3)
		{
			std::cout << "The Winner is Player 1(X): ";
		}
		else if (count == 1)
		{
			std::cout << "The Game is a tie: ";
		}
	}
};